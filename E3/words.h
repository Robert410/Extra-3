#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

using namespace std;
/// <summary>
/// the tamplate will store the posibility of the ouput for the solution creating with the paths
/// </summary>
template <typename T>
class Solution {
public:
    void createMap(vector<string>& wordList);

    T findSolutionPath(string beginWord, string endWord, vector<string>& wordList);

private:
    unordered_set<string> dict, cop;

    void findChildren(string word, unordered_set<string>& next, unordered_set<string>& dict, unordered_map<string, vector<string>>& PathTarget);

    void genSolutionPath(string beginWord, string endWord, unordered_map<string, vector<string>>& PathTarget, vector<string>& ChildrenWords, T& finalSol);
};


template <typename T>
void Solution<T>::createMap(vector<string>& wordList) {
    /// <summary>
    /// Take the dictionary and put it in a unordered_set and make a copy of it ( because i will eleiminate words from the dic and after the game i have to be sure that my dictionary is still fine)
    /// </summary>
    dict = unordered_set<string>(wordList.begin(), wordList.end());
    cop = dict;
}



template <typename T>
T Solution<T>::findSolutionPath(string beginWord, string endWord, vector<string>& wordList) {
    /// <summary>
    /// i take all the possibilities, create all the path, check every next until i reach the endWord, after i call the function of getSoultionPath, and return de T elem ( finalSol) where the solution will be stored
    /// </summary>
    unordered_set<string>  current, next;

    unordered_map<string, vector<string>> PathTarget;
    T finalSol;
    vector<string> ChildrenWords;
    current.insert(beginWord);
    ChildrenWords.push_back(beginWord);
    while (true) {
        for (string word : current) {
            dict.erase(word);
        }
        for (string word : current) {
            findChildren(word, next, dict, PathTarget);
        }
        if (next.empty()) {
            break;
        }
        if (next.find(endWord) != next.end()) {
            genSolutionPath(beginWord, endWord, PathTarget, ChildrenWords, finalSol);
            break;
        }
        current.clear();
        swap(current, next);
    }
    dict = cop;
    /*for (auto i : PathTarget) {
        cout << i.first << " ";
    }*/
    return finalSol;
}


template <typename T>
void Solution<T>::findChildren(string word, unordered_set<string>& next, unordered_set<string>& dict, unordered_map<string, vector<string>>& PathTarget) {
    /// <summary>
    /// Take a word, create its children and put them in the graph as: parent->childrens (like a tree) 
    /// </summary>
    string parent = word;
    for (int i = 0; i < word.size(); i++) {
        char t = word[i];
        for (int j = 0; j < 26; j++) {
            word[i] = 'a' + j;
            if (dict.find(word) != dict.end()) {
                next.insert(word);
                PathTarget[parent].push_back(word);
            }
        }
        word[i] = t;
    }
}


template <typename T>
void Solution<T>::genSolutionPath(string beginWord, string endWord, unordered_map<string, vector<string>>& PathTarget, vector<string>& ChildrenWords, T& finalSol) {
    /// <summary>
    /// i go through  my graph and take all possible ways from beginWord to endWord and push them into an T elemnt 
    /// </summary>
    if (beginWord == endWord) {
        finalSol.push_back(ChildrenWords);
    }
    else {
        for (string child : PathTarget[beginWord]) {
            ChildrenWords.push_back(child);
            genSolutionPath(child, endWord, PathTarget, ChildrenWords, finalSol);
            ChildrenWords.pop_back();
        }
    }
}
