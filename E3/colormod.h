#include <ostream>
namespace Color {
    enum Code {
        /// <summary>
        /// a class with some important colors for the output
        /// </summary>
        FG_RED = 31,
        FG_GREEN = 32,
        FG_BLUE = 34,
        FG_DEFAULT = 39,
        BG_RED = 41,
        BG_GREEN = 42,
        BG_BLUE = 44,
        BG_DEFAULT = 49
    };
    class Modifier {
        Code code;
    public:
        Modifier(Code pCode) : code(pCode) {}
        friend std::ostream&
            operator<<(std::ostream& os, const Modifier& mod) {
            /// <summary>
            /// override the out operation
            /// </summary>
            /// <param name="os"the output></param>
            /// <param name="mod">the color</param>
            return os << "\033[" << mod.code << "m";
        }
    };
}